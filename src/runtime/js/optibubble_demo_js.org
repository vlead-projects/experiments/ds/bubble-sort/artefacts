#+TITLE:
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction  
  This document builds the functionality with JavaScript to
  add behaviour/interactivity to the "Simple Bubble Sort Exercise"
  demonstration artefact(HTML).

* Bubble sort class

+ Creates Bubble_sort class, which has all the variables required during its running
+ We use "let" to create an object of this class
+ iteratro1 -> Used to keep track while traversing the array
+ iterator2 -> Used to keep track while traversing the array(iterator1 + 1)
+ finished -> Determines whether or not the sort is finished
+ action -> Determines the current action being performed
+ fn_name -> Name of the function (Bubble sort here)
+ card -> Variable that corresponds to DOM elements that represent the array
+ comparisons -> Counts the number of comparisons
+ swaps -> Counts the number of swaps
+ operation -> Determined current operation
+ interval -> Varialble corresponding to setting and unsetting the slider time interval
+ num -> Variable that stores the array
+ Array has been initialized to contain all zero values to allocate the memory at the start itse
+ Creating an object of the Bubble_sort using "let"
#+NAME: demo-universal-class
#+BEGIN_SRC js

class Bubble_sort{
  constructor(){
    this.iterator1 = 0;
    this.iterator2 = 0;
    this.finished = false;
    this.numOfCards = 8;
    this.prev = -1;
    this.action = 0;
    this.fn_name = "";
    this.card;
    this.comparisons = 0;
    this.swaps = 0;
    this.operation = "";
    this.interval = 0;
    this.num = [];
    this.flag = 0;
  };
};
let bubble_artefact = new Bubble_sort();

#+END_SRC

* Main Function
A function that performs the body onload functions
#+NAME: main-functions
#+BEGIN_SRC js
function main_functions()
{ 
  randomise();
  handlers();
};
document.body.onload = function() {main_functions();}
#+END_SRC

* Adds all event handlers
A function that adds all the event handlers to the html
document.
#+NAME: handlers
#+BEGIN_SRC js
  function handlers(){
  document.getElementById("next").onclick = function() {start_sort();};
  document.getElementById("interval").onchange = function() {change_interval();};
  document.getElementById("interval").oninput = function() {change_interval();};
  document.getElementById("reset").onclick = function() {reload();};
  document.getElementById("pause").onclick = function() {pause();};
  };
#+END_SRC

* Function Randomise

+ Creates a randomised array of a defined size
+ Places these random values inside DOM cards
#+NAME: demo-randomise
#+BEGIN_SRC js

function randomise()
{
  bubble_artefact.action = 0;
  var classToFill = document.getElementById("cards");
    for (var i = 0; i < bubble_artefact.numOfCards; i++){
      bubble_artefact.num[i] = Math.floor(Math.random() * 90 + 10);
      var temp = document.createElement("div");
      temp.className = "card";
      temp.innerHTML = bubble_artefact.num[i];
      temp.style.fontStyle = "normal";
      temp.style.color = "white";
      classToFill.appendChild(temp);
    }
  bubble_artefact.flag=0;
  document.getElementById("pause").disabled = true;
  document.getElementById("pause").style.backgroundColor = "grey";
};

#+END_SRC

* Function Change Interval

+ Supports changing time interval when the slider is changed in the webpage
+ Also, changes the background colour of Pause button if required
#+NAME: demo-slider
#+BEGIN_SRC js

function change_interval()
{
  if(bubble_artefact.prev == -1 && (bubble_artefact.action == 1 || bubble_artefact.action == -1)){
    if(bubble_artefact.interval != 0)
      clearInterval(bubble_artefact.interval);
    if(document.getElementById("interval").value != 100)
    {
      if(bubble_artefact.fn_name > "") 
          bubble_artefact.interval = setInterval(next_step, 2600-document.getElementById("interval").value); 
      document.getElementById("pause").style.backgroundColor = "#288ec8";  
    }
    else
      document.getElementById("pause").style.backgroundColor = "grey";  
  }
};

#+END_SRC

* Function compare

+ Compares two given numbers given as input to it
+ Highlights numbers that are under consideration by changing their background colour
+ Returns True if the swap has to be performed
+ Return False otherwise
#+NAME: demo-compare
#+BEGIN_SRC js
function compare(i, j)
{
  bubble_artefact.comparisons++;
  for(var n = 0; n < bubble_artefact.numOfCards; n++)
  {
    if(n == i || n == j) 
    {
      if(bubble_artefact.card[n].style.backgroundColor != "grey" || !bubble_artefact.flag) 
        bubble_artefact.card[n].style.backgroundColor = "#a4c652"; 
    }
    else
    { 
      if(bubble_artefact.card[n].style.backgroundColor != "grey" && !bubble_artefact.flag)
      bubble_artefact.card[n].style.backgroundColor = "#288ec8";
    }
  }
  document.getElementById("ins").innerHTML = "<p>Comparisons: " + bubble_artefact.comparisons + " | Swaps: " + bubble_artefact.swaps + "</p><p>Comparing " + bubble_artefact.card[i].innerHTML + " and " + bubble_artefact.card[j].innerHTML + "...</p>";
  if(eval(bubble_artefact.card[j].innerHTML) < eval(bubble_artefact.card[i].innerHTML))
  {
    document.getElementById("ins").innerHTML += "<p>" + bubble_artefact.operation + " required" + " : " + "Swapping " + bubble_artefact.card[i].innerHTML + " and " + bubble_artefact.card[j].innerHTML + "</p>"
    return true;
  }
  else
  {
    document.getElementById("ins").innerHTML += "<p>" + bubble_artefact.operation + " not required</p>"
    return false;
  }
};

#+END_SRC

* Function Swap

+ Swaps the two elements whose indices are given as input
#+NAME: demo-swap
#+BEGIN_SRC js
function swap(i, j)
{
  bubble_artefact.swaps++;
  var temp;
  temp = eval(bubble_artefact.card[j].innerHTML);
  bubble_artefact.card[j].innerHTML = eval(bubble_artefact.card[i].innerHTML);
  bubble_artefact.card[i].innerHTML = temp;
};


#+END_SRC

* Function Opti_Bubble

+ Main driving function for this sorting algorithm
+ Updates the values of iterator1 and iterator2 and checks for termination conditions
#+NAME: demo-optibubble
#+BEGIN_SRC js

function opti_bubble()
{
  if(bubble_artefact.iterator1 < bubble_artefact.end)
  {
    bubble_artefact.iterator1++;
    bubble_artefact.iterator2++;
  }
  else
  {
    if(bubble_artefact.finished)
    {
      if(document.getElementById("interval").value != 100)
      {
        clearInterval(bubble_artefact.interval);
        bubble_artefact.interval = 0;
      }
      document.getElementById("ins").innerHTML = "<h3>The sort is complete - there were " + bubble_artefact.comparisons + " comparisons and " + bubble_artefact.swaps + " swaps.</h3>";
      document.getElementById("next").style.backgroundColor = "grey";
      document.getElementById("next").disabled = true;
      document.getElementById("pause").style.backgroundColor = "grey";
      document.getElementById("pause").disabled = true;
      bubble_artefact.iterator2 = 0;
    }
    else
    {
      bubble_artefact.finished = true;
      bubble_artefact.iterator1 = 0;
      bubble_artefact.iterator2 = 1;  
      bubble_artefact.card[bubble_artefact.end+1].style.backgroundColor = "grey";
      bubble_artefact.end--;
    }
  }
};
#+END_SRC

* Function next step

+ Performs operations based on a given action
+ Highlights required elements
+ Begins a new iteration at the end of a new one (until termination)
#+NAME: demo-next-step
#+BEGIN_SRC js
function next_step()
{
  if(bubble_artefact.action == 1)
  {
    if(compare(bubble_artefact.iterator1, bubble_artefact.iterator2))
        bubble_artefact.action = -1;
    else
        window[bubble_artefact.fn_name]();
  }
  else
  {
    bubble_artefact.action = 1;
    if(bubble_artefact.fn_name == "opti_bubble") { swap(bubble_artefact.iterator1, bubble_artefact.iterator2); }
    bubble_artefact.finished = false;
    window[bubble_artefact.fn_name]();
  }
};

#+END_SRC

* Function Pause

+ Called when Pause button is clicked
+ Makes the value of interval the minimum possible, so that the delay (2600-100) is maximum.
#+NAME: demo-pause
#+BEGIN_SRC js

function pause(){
  if(bubble_artefact.prev == -1){
    bubble_artefact.prev = document.getElementById("interval").value;
    if(bubble_artefact.interval != 0) 
      clearInterval(bubble_artefact.interval);
    document.getElementById("pause").value = "Play";
  }else{
    bubble_artefact.prev = -1;
    if(bubble_artefact.fn_name > "") 
      bubble_artefact.interval = setInterval(next_step, 2600-document.getElementById("interval").value);
    document.getElementById("pause").value = "Pause";
  }
};
#+END_SRC

* Function Start sort

+ Called when Start is clicked on the webpage
+ Starts the sorting regime
+ Initializes variables that are going to be used
+ Calls the required function based on the sorting algorithm
+ Set the interval by calling change_interval
#+NAME: demo-start-sort
#+BEGIN_SRC js

function start_sort()
{
  if(bubble_artefact.interval != 0) { clearInterval(bubble_artefact.interval); bubble_artefact.interval = 0; }
  document.getElementById("comment-box-bigger").style.visibility = "visible";
  bubble_artefact.card = document.querySelectorAll('.card');
  bubble_artefact.action = 1;
  bubble_artefact.finished = true;
  bubble_artefact.comparisons = 0;
  bubble_artefact.swaps = 0;
  bubble_artefact.fn_name = "opti_bubble";
  
  bubble_artefact.iterator1 = 0;
  bubble_artefact.iterator2 = 1;
  bubble_artefact.end = bubble_artefact.numOfCards-2;
  bubble_artefact.operation = "Swap";
  next_step();

  document.getElementById("next").onclick = function() { next_step(); };
  document.getElementById("next").value = "Next";
  document.getElementById("pause").disabled = false;
  document.getElementById("pause").style.backgroundColor = "#288ec8";
  if(document.getElementById("interval").value == 100)
  {
    document.getElementById("next").disabled = false;
    document.getElementById("pause").style.visibility = "hidden";
  }
  else
  {
    document.getElementById("pause").style.visibility = "visible";
    if(bubble_artefact.interval == 0)
        bubble_artefact.interval = setInterval(next_step, 2600-document.getElementById("interval").value);
    else
    {
      clearInterval(bubble_artefact.interval);
      bubble_artefact.interval = 0;
    }
  }

};
#+END_SRC
* Function reload

+ Used to reload the artefact when Reset button is clicked
#+NAME: demo-reload
#+BEGIN_SRC js

function reload(){
  location.reload(true);
};

#+END_SRC

* Tangle
#+BEGIN_SRC js :tangle optibubble_demo.js :eval no :noweb yes
<<demo-universal-class>>
<<main-functions>>
<<handlers>>
<<demo-randomise>>
<<demo-slider>>
<<demo-compare>>
<<demo-swap>>
<<demo-optibubble>>
<<demo-next-step>>
<<demo-pause>>
<<demo-start-sort>>
<<demo-reload>>
#+END_SRC
